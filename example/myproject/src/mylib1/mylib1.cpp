#include "myproject/mylib1.hpp"

#include <Eigen/Core>

void test_mylib1()
{
  std::cout << "Hello from test_mylib1!\n";
}

void test_mylib1_with_Eigen()
{
  Eigen::MatrixXd mat(1,1);
  mat << 1;
  
  std::cout << "Hello from test_mylib" << mat(0,0) << " with Eigen!\n";
}
